# Projeto 2 Grupo 2 - Luta 1 vs 1


## Universidade Federal de Goiás - ADSI2015/2 - Professor Akira


#### Descrição

* Jogo multiusuário podendo ter jogadores humanos ou IA;

* O Jogo é iniciado quando 2 jogadores conectam;

* Cada lutador começa o Round com 100 de life e o vencedor é quem no fim do Round estiver com mais life;

* O jogo terá 3 rounds vencendo o jogador com 2 vitórias;

* Cada lutador ataca uma vez, seu ataque pode ser também uma defesa;