package cliente;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;

import Interfaces.JogadorInt;
import Interfaces.ServidorInt;

public class Jogador implements JogadorInt {

  static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
  static Registry registry;
  static ServidorInt servidor;
  static Jogador jogador;

  private double life = 100;
  private String nome;
  private String estado = Jogador.INERCIA;
  private int numJogador;

  static {
    try {
      registry = LocateRegistry.getRegistry("localhost");
      servidor = (ServidorInt) registry.lookup("Jogo");
    } catch (Exception e) {
      System.err.println(e);
      System.exit(0);
    }
  }

  /**
   * M�todo de iniciar o do Registry foi usado com base no projeto do Grupo 1
   */
  public static void main(String[] args) throws Exception {
    try {
      System.out.println("Informe seu nome: ");
      jogador = new Jogador();
      jogador.setNome(reader.readLine());
      JogadorInt stub = (JogadorInt) UnicastRemoteObject.exportObject(jogador, 0);
      Registry registry = LocateRegistry.createRegistry(new Random().nextInt(1000) + 6000);
      registry.bind("Jogador_" + jogador.getNome(), stub);
      servidor.registrar(jogador);
    } catch (Exception e) {
      System.err.println("Server exception: " + e.toString());
      e.printStackTrace();
    }
    while (true) {
      while (servidor.getJogoIniciado()) {
        if (servidor.getVez(jogador.getNumJogador())) {
          jogador.receberMensagem("Sua vez, d� um golpe em seu oponente!!");
          servidor.receberJogada(reader.readLine(), jogador.getNumJogador());
          servidor.getLifes();
          jogador.receberMensagem("Aguarde sua vez.");
        }
      }
    }
  }

  /**
   * M�todo de retornar o Life do jogador
   */
  @Override
  public double getLife() throws RemoteException {
    return life;
  }

  /**
   * M�todo para receber o golpe
   */
  @Override
  public void receberGolpe(float golpe) {
    life = life - golpe;
  }

  /**
   * M�todo para receber a mensagem e printar no console
   */
  @Override
  public void receberMensagem(String mensagem) throws RemoteException {
    System.out.println(mensagem);
  }

  @Override
  public String getNome() throws RemoteException {
    return nome;
  }

  @Override
  public String getEstado() throws RemoteException {
    return estado;
  }

  @Override
  public void setEstado(String estado) throws RemoteException {
    this.estado = estado;

  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  @Override
  public int getNumJogador() {
    return numJogador;
  }

  @Override
  public void setNumJogador(int numJogador) throws RemoteException {
    this.numJogador = numJogador;
  }

}
