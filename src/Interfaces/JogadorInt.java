package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Humberto Miranda
 */
public interface JogadorInt extends Remote {

  public final String INERCIA = "INERCIA";
  public final String DEFESA = "DEFESA";

  public double getLife() throws RemoteException;

  public void receberMensagem(String mensagem) throws RemoteException;

  public String getNome() throws RemoteException;

  public String getEstado() throws RemoteException;

  public void setEstado(String estado) throws RemoteException;

  public int getNumJogador() throws RemoteException;

  public void setNumJogador(int numJogador) throws RemoteException;

  void receberGolpe(float golpe) throws RemoteException;

}
