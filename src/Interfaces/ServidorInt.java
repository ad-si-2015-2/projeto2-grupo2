package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Humberto Miranda
 */
public interface ServidorInt extends Remote {

  public void registrar(JogadorInt cliente) throws RemoteException;

  public void receberJogada(String comando, int numJogador) throws RemoteException;

  public boolean getJogoIniciado() throws RemoteException;

  public void getLifes() throws RemoteException;

  public boolean getVez(int numJogador) throws RemoteException;
}
