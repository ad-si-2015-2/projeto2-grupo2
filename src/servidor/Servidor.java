package servidor;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import Interfaces.JogadorInt;
import Interfaces.ServidorInt;
import cliente.Jogador;

/**
 * @author Humberto Miranda
 */
public class Servidor implements ServidorInt {

  ArrayList<JogadorInt> jogadores = new ArrayList<JogadorInt>();
  boolean jogoIniciado = false;
  private int vez;
  Jogada jogada;

  @Override
  public void registrar(JogadorInt jogador) throws RemoteException {
    if (jogadores.size() < 2) {
      jogadores.add(jogador);
      jogador.receberMensagem("Voce esta cadastrado no Servidor.");
      jogador.receberMensagem(comandosJogo());
      jogador.receberMensagem("Aguarde o jogo come�ar " + jogador.getNome());
      if (jogadores.size() == 2) {
        notificaJogadores("\r\n" + "|===============================================================|\r\n"
            + "|-----------------      O jogo ser� iniciado    ----------------|\r\n"
            + "|===============================================================|\r\n");

        iniciaJogo();
        jogador.setNumJogador(1);
      } else {
        jogador.setNumJogador(0);
      }
    } else {
      jogador.receberMensagem("Sem vaga para mais jogadores.\n" + "Voce nao foi registrado no servidor.");
    }
  }

  public void setVez(int vez) {
    this.vez = vez;
  }

  public int getVez() {
    return vez;
  }

  public void iniciaJogo() throws RemoteException {
    vez = 0;
    jogoIniciado = true;
  }

  public static void main(String[] args) {
    try {
      Servidor obj = new Servidor();
      ServidorInt stub = (ServidorInt) UnicastRemoteObject.exportObject(obj, 0);

      Registry registry = LocateRegistry.createRegistry(1099);
      registry.bind("Jogo", stub);

      System.out.println("Servidor iniciado...");

    } catch (Exception e) {
      System.err.println("Server exception: " + e.toString());
      e.printStackTrace();
    }
  }

  private int getInimigo() {
    return vez == 0 ? 1 : 0;
  }

  @Override
  public void receberJogada(String comando, int numJogador) throws RemoteException {
    comando = comando.toUpperCase();

    JogadorInt inimigo = jogadores.get(getInimigo());
    JogadorInt atacante = jogadores.get(numJogador);

    if (numJogador == vez) {
      for (Jogada jogada : Jogada.values()) {
        if (comando.equals(jogada.getAtalho())) {
          if (comando.equals("D")) {
            atacante.setEstado(Jogador.DEFESA);
          } else {
            if (inimigo.getEstado().equals(Jogador.DEFESA)) {
              inimigo.receberGolpe(jogada.randomHitDefesa());
            } else {
              inimigo.receberGolpe(jogada.randomHitIncercia());
            }
            atacante.setEstado(Jogador.INERCIA);
          }
          inimigo.receberMensagem(jogada.getMensagem());
        }
      }
      verificarDerrota(inimigo, atacante);
    }
  }

  private void verificarDerrota(JogadorInt inimigo, JogadorInt atacante) throws RemoteException {
    if (inimigo.getLife() <= 0) {
      inimigo.receberMensagem("Voce perdeu!");
      atacante.receberMensagem("Parabens voce venceu a luta");
    } else {
      getLifes();
      vez = vez();
    }
  }

  private int vez() {
    return vez > 0 ? 0 : 1;
  }

  public void notificaJogadores(String mensagem) throws RemoteException {
   System.out.println(mensagem);
  }

  @Override
  public boolean getJogoIniciado() throws RemoteException {
    return jogoIniciado;
  }

  @Override
  public void getLifes() throws RemoteException {
    StringBuilder sb = new StringBuilder();
    for (JogadorInt jogador : jogadores) {
      sb.append(String.format("%s: %s\n", jogador.getNome(), jogador.getLife()));
    }
    notificaJogadores(sb.toString());
  }

  @Override
  public boolean getVez(int numJogador) throws RemoteException {
    return numJogador == vez;
  }

  public static String comandosJogo() {
    return "\r\n" + "|===============================================================|\r\n"
        + "|-----------------         Comandos do Jogo     ----------------|\r\n"
        + "|                                                               |\r\n"
        + "| ##############################################################|\r\n"
        + "| #################                            #################|\r\n"
        + "| ################# C -> Chute                 #################|\r\n"
        + "| ################# S -> Soco                  #################|\r\n"
        + "| ################# D -> Defesa                #################|\r\n"
        + "| ################# E -> Esquiva               #################|\r\n"
        + "| #################                            #################|\r\n"
        + "| #################                            #################|\r\n"
        + "| ##############################################################|\r\n"
        + "|===============================================================|\r\n\r\n";
  }
}
