package servidor;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Enum que representa as jogadas possíveis.
 * @author Dayan Costa
 */
public enum Jogada {

  SOCO("S", "Soco", "Voce levou um soco!"),
  CHUTE("C", "Chute", "Voce levou um chute!"),
  DEFESA("D", "Defesa", "Adiversario em modo de defesa!"),
  ESQUIVA("E", "Esquiva", "Jogador esquivou-se");

  private String atalho;
  private String descricao;
  private String mensagem;

  private Jogada(String atalho, String descricao, String mensagem) {
    this.atalho = atalho;
    this.descricao = descricao;
    this.mensagem = mensagem;
  }

  /**
   * @return hit aleatório entre 5 e 15 quando o inimigo estiver em estado de inércia
   */
  public float randomHitIncercia() {
    int max = 15;
    int min = 7;
    return new Random().nextInt(max - min + 1) + min;
  }

  /**
   * @return hit aleatório entre 1 e 7 quando o inimigo estiver em estado de defesa
   */
  public float randomHitDefesa() {
    int max = 7;
    int min = 1;
    return new Random().nextInt(max - min + 1) + min;
  }

  public String getAtalho() {
    return atalho;
  }

  public String getDescricao() {
    return descricao;
  }

  public String getMensagem() {
    return mensagem;
  }

  public static final List<Jogada> getJogadas() {
    return Collections.unmodifiableList(Arrays.asList(values()));
  }

  public static String random() {
    return getJogadas().get(new Random().nextInt(getJogadas().size())).getAtalho();
  }
}
